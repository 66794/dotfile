" Use Unix as the standard file type
set ffs=unix,dos,mac

" Makefile Setup
autocmd Filetype make setlocal noexpandtab " use tab char instead of n spaces
