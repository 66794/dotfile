source ~/.vim/filetype.vim
source ~/.vim/security.vim

" move viminfo away from home
set viminfo+=n$XDG_DATA_HOME/vim/viminfo " move viminfo file away from home
" move netrwhist away from home
let g:netrw_home='$XDG_DATA_HOME/vim/'

" Vi compability
if &compatible
  set nocompatible
endif

set backspace=indent,eol,start
set ruler " show the line and column number of the cursor position

syntax on

silent! colorscheme dracula " try 3rd-party theme

" set black background instead of inheriting the background color of the terminal
highlight Normal ctermbg=16
highlight Comment ctermfg=1 " these values may vary based on the current theme

set number " line number counter

set autoindent " copy the identation of the current line when starting a new line
set smartindent " behave like cident

" tabs
set expandtab " replace tabs by white spaces
set smarttab " a tab in front of a line inserts blanks according to shiftwidth, tabstop or softtabstop
set shiftwidth=4
set tabstop=4
set softtabstop=4

set wildmenu " completion enhancement

" Set to auto read when a file is changed from the outside
set autoread
au FocusGained,BufEnter * checktime

set so=0 " set n lines to the cursor - when moving vertically using j/k

" searching
set ignorecase
set smartcase " override the ignorecase option if the search pattern contains upper case characters
set hlsearch " highlighting matching pattern
set incsearch " go to the first matching pattern when typing a search command
" set showmatch " when a bracket is inserted, briefly jump to the matching one

set lazyredraw " Don't redraw while executing macros (good performance config)

set magic " For regular expressions. For example "." matches any character while "\." matches a dot.

set wrap " no horizontal scroll bar
