# xdg path
export XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR:="/tmp/xdg_runtime_dir"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}

# directory for zshell config files
export ZDOTDIR="$XDG_CONFIG_HOME/zshell"
export HISTFILE="$XDG_DATA_HOME/zhist"

# less history file
export LESSHISTSIZE=-
export LESSHISTFILE='/tmp/lesshistfile'
#lesskey > /dev/null 2>&1

# colorful less (useful for man-page)
export LESS='-R --use-color -Dd+r$Du+b'

# x
export XINITRC="$XDG_CONFIG_HOME/xinit/xinitrc"
export XAUTHORITY="$XDG_DATA_HOME/.Xauthority"

# gpg dotdir
export GNUPGHOME="$XDG_DATA_HOME/gnupg"

# audio output device
sysctl hw.snd.default_unit=6 1>/dev/null
