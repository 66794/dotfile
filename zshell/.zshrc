# Lines configured by zsh-newuser-install
HISTSIZE=10000
SAVEHIST=10000
# vi mode
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '$ZDOTDIR/.zshrc'

# specify comp dump file path
autoload -Uz compinit
compinit
# End of lines added by compinstall

# aliases
alias ls='ls -la --color=auto'
alias cat='cat -n'
alias grep='grep -n --color=auto'
alias nv='nvim -p'
alias python='python3.9'

# prompt
setopt PROMPT_SUBST
PROMPT='%K{52}%F{white}[%f%k%K{52}%F{white}%d%f%k%K{52}%F{white}]%f%k%K{36}%F{white}$(git branch 2>/dev/null | grep "*" | sed "s/^..../\[/g" | sed "s/$/\]/g")%f%k'

# start wm at login if on a specific tty
if [[ "$(tty)" = "/dev/ttyv0" ]]; then
    startx
fi

# include fancy syntax
source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
