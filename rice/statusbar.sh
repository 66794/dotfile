while true; do
    date="$(date "+%Y-%m-%d")"
    time="$(date "+%H:%M:%S")"
    memo="$(vmstat -H | sed -n 3p | awk '{print $5}' | sed "s/...$//g")"
    cpu_temp="$(sysctl dev.cpu.0.temperature | awk '{print $2}')"
    volu="$(mixer vol | awk '{print $7}')"

    xsetroot -name "[$volu]  [$cpu_temp]  [$memo]  [$date]  [$time]"

    sleep 1
done
