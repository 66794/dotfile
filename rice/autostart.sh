# night color
redshift -o 2400

# wallpaper
sh ~/.config/feh/.fehbg

while true; do
    date="$(date -v+0m -v+0y | awk '{print $1, $2, $3, $4}')"
    mem="$(freecolor -m -o | sed -n 2p | awk '{print $3}')"
    cpu_temp="$(sysctl dev.cpu.0.temperature | awk '{print $2}')"
    vol="$(mixer vol | awk '{print $7}')"

    xsetroot -name "[$vol]  [$cpu_temp]  [$mem/15]  [$date]"

    sleep 1
done
