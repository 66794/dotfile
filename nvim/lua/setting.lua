-- aliases
local o = vim.opt
local c = vim.cmd

-- local variables
local indentation_width = 4

-- general
o.compatible = false
o.ruler = true -- status line
o.wildignore = { '*.o', '*.out', '.git' } -- wildcards will ignore these file types
o.autoread = true -- auto read files changed from the outside
c('au FocusGained,BufEnter * checktime') -- auto read files changed from the outside
o.wildmenu = true -- pop up menu when autocompleting
o.number = true -- show line number
o.wrapscan = true -- wrap searches from top to bottom of the file
o.textwidth = 0 -- never wrap lines
o.scrolloff = 0 -- don't start scrolling when the cursor is near the top/bottom

-- indentation options
o.expandtab = true -- replace tab characters by <indentation_width> times white space
o.tabstop = indentation_width -- how much white spaces count as a tab and vice-versa
o.softtabstop = indentation_width -- same thing as tabstop but for editing
o.smarttab = true

o.shiftwidth = indentation_width
o.autoindent = true
o.smartindent = true

-- search
o.ignorecase = true -- don't differ uppercase and lowercase
o.hlsearch = true -- highlight all search matches
o.incsearch = true -- jump screen to first search match
o.showmatch = true -- show matching pontuation
o.magic = true -- For regular expressions turn magic on

-- theme
c('syntax enable')
c('highlight Normal ctermbg=16')
c('highlight Comment ctermfg=1')
