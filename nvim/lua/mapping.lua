-- alias
local keymap = vim.api.nvim_set_keymap

-- local variables
local modkey = "<space>"
local opt = {noremap = true, silent = true}

-- (re)mapping keybinding
keymap("", modkey, "<Nop>", opt)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- c header macro
keymap("n", modkey.."h", "<esc>ggO#ifndef A<cr>#define A<cr><cr><esc>Go#endif<esc>:1,2s/A//g<Left><Left>", opt)
-- c main function
keymap("n", modkey.."m", "i#include <iostream><cr><cr>int main(int argc, char** argv) {<cr>return 0;<cr>}<esc>kko", opt)
-- std::cout
keymap("n", modkey.."c", "<esc>istd::cout << << std::endl;<esc>bbbbi", opt)
