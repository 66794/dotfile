local c = vim.cmd

-- include
c('source ~/.config/nvim/vim/init.vim') -- yet not converted to lua settings
c('source ~/.config/nvim/lua/setting.lua') -- mostly vimrc-like settings
c('source ~/.config/nvim/lua/mapping.lua') -- mapping and macros
